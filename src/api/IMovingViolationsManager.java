package api;

import java.time.LocalDateTime;
import java.time.LocalTime;

import model.data_structures.IQueue;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.hashTableSeparateChaining;
import model.vo.AddressIdVO;
import model.vo.DateVO;
import model.vo.FineamtVO;
import model.vo.GeoVO;
import model.vo.LocationVO;
import model.vo.RankingFH;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 */
	void loadMovingViolations(int pRespuestaCuat, int pMetodo);

	MaxColaPrioridad<RankingFH> rankingFranjasHorarias(int n);
	
	hashTableSeparateChaining<String, IQueue<VOMovingViolations>> ordenLocalGeog(double pXCoord, double pYcoord);
	
	RedBlackBST<String, DateVO>buscarInfRangoFechas(String pFechaInicial, String pFechaFinal);
	
	AddressIdVO getLocalInfo(Integer pAddressId);
	
	IQueue<GeoVO> getLocalRanking(int n);
	
	void getASCIITable();
	
	RedBlackBST<LocalTime,IQueue<VOMovingViolations>> darInfraccionesPorRango(LocalTime pHoraInicial, LocalTime pHoraFinal);
	
	RedBlackBST<String,IQueue<VOMovingViolations>> consultarPorLocalizacion(double x, double y);
	
	MaxColaPrioridad<Queue<VOMovingViolations>> darRankingInfracciones(int pViolationCode);
	
	RedBlackBST<Integer, FineamtVO> darFranjasConValAcumulad(double aI, double bF);

}
