package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import com.opencsv.CSVReader;
import api.IMovingViolationsManager;
import model.vo.AddressIdVO;
import model.vo.DateVO;
import model.vo.FineamtVO;
import model.vo.GeoVO;
import model.vo.LocationVO;
import model.vo.RankingFH;
import model.vo.VOMovingViolations;
import model.data_structures.IQueue;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.hashTableLinearProbing;
import model.data_structures.hashTableSeparateChaining;
import model.util.Sort;

public class MovingViolationsManager implements IMovingViolationsManager {

	private Queue<VOMovingViolations> queueInfracciones = new Queue<VOMovingViolations>();

	private VOMovingViolations[] listaInfracciones;

	private MaxColaPrioridad<Queue<VOMovingViolations>> maxColaP = new MaxColaPrioridad<Queue<VOMovingViolations>>();

	private RedBlackBST<String,IQueue<VOMovingViolations>> arbolBST = new RedBlackBST<String,IQueue<VOMovingViolations>>();

	private RedBlackBST<LocalTime,IQueue<VOMovingViolations>> arbolBSTReqC = new RedBlackBST<LocalTime,IQueue<VOMovingViolations>>(); 

	private hashTableSeparateChaining<String,IQueue<VOMovingViolations>>  separateChainning =new hashTableSeparateChaining<>();

	private double media=0.0;


	/////////////
	private RedBlackBST<String,IQueue<String>> arbolBSTReqC4 = new RedBlackBST<String,IQueue<String>>(); 

	/////////////

	private hashTableLinearProbing<Integer,AddressIdVO > hash1C = new hashTableLinearProbing<>();

	private MaxColaPrioridad<GeoVO> cola3C = new MaxColaPrioridad<>();

	//Scanner sc = new Scanner(System.in);

	int determinante = 0;

	String objectId = "";
	String location = "";
	String ticketIssueDate ="";
	String totalPaid ="";
	String accidentIndicator ="";
	String violationDescription ="";
	String sumaFINEAMT ="";
	String violationCode ="";
	String adressId = "";
	String streetSegId = "";
	String PENALTY1 =""; 
	String PENALTY2 ="";
	String xCoord = "0";
	String yCoord = "0";

	double primerMes=0;
	double segundoMes=0;
	double tercerMes=0;
	double cuartoMes=0;
	double quintoMes=0;
	double sextoMes=0;

	int cero = 0;
	int uno = 0;
	int dos = 0;
	int tres = 0;
	int cuatro = 0;
	int cinco = 0;


	double xCoordMin = 999999999;
	double yCoordMin = 999999999;

	double xCoordMax = 0;
	double yCoordMax =0;

	public Queue<VOMovingViolations> darQueue() {

		return queueInfracciones;

	}

	int pos = 0;

	public void loadMovingViolations(int pRespuestaSem, int pMetodo) {

		try {

			String[] archCSV = new String[6];
			String[] meses = new String[6];

			Integer[] cantidades = new Integer[6];

			if(pRespuestaSem == 1) {

				archCSV[0] = "./data/Moving_Violations_Issued_in_January_2018.csv";
				archCSV[1] = "./data/Moving_Violations_Issued_in_February_2018.csv";
				archCSV[2] = "./data/Moving_Violations_Issued_in_March_2018.csv";
				archCSV[3] = "./data/Moving_Violations_Issued_in_April_2018.csv";
				archCSV[4] = "./data/Moving_Violations_Issued_in_May_2018.csv";
				archCSV[5] = "./data/Moving_Violations_Issued_in_June_2018.csv";

				meses[0] = "Enero";
				meses[1] = "Febrero";
				meses[2] = "Marzo";
				meses[3] = "Abril";
				meses[4] = "Mayo";
				meses[5] = "Junio";

				listaInfracciones = new VOMovingViolations[599207];

				determinante = 0;


			} else if(pRespuestaSem == 2) {

				archCSV[0] = "./data/Moving_Violations_Issued_in_July_2018.csv";
				archCSV[1] = "./data/Moving_Violations_Issued_in_August_2018.csv";
				archCSV[2] = "./data/Moving_Violations_Issued_in_September_2018.csv";
				archCSV[3] = "./data/Moving_Violations_Issued_in_October_2018.csv";
				archCSV[4] = "./data/Moving_Violations_Issued_in_November_2018.csv";
				archCSV[5] = "./data/Moving_Violations_Issued_in_December_2018.csv";

				meses[0] = "Julio";
				meses[1] = "Agosto";
				meses[2] = "Septiembre";
				meses[3] = "Octubre";
				meses[4] = "Noviembre";
				meses[5] = "Diciembre";

				listaInfracciones = new VOMovingViolations[664489];

				determinante = 1;
			} 

			int contTotal = 0;

			for(int i = 0; i < 6; i++) {

				int cont = 0;


				CSVReader csvReader = new CSVReader(new FileReader(archCSV[i]));
				String[] fila = null;
				csvReader.readNext();
				while((fila = csvReader.readNext())!= null ) {

					if(pRespuestaSem==2 && (i==3||i==4||i==5))
					{
						objectId = fila[0];
						location = fila[2];
						ticketIssueDate = fila[14];
						sumaFINEAMT = fila[8];
						totalPaid = fila[9];
						accidentIndicator = fila[12];
						violationDescription = fila[16];
						violationCode = fila[15];
						adressId = fila[3];
						streetSegId = fila[4];
						PENALTY1 = fila[10];
						PENALTY2 = fila[11];
						xCoord = fila[5];
						yCoord = fila[6];
					}else {
					objectId = fila[0];
					location = fila[2];
					ticketIssueDate = fila[13];
					sumaFINEAMT = fila[8];
					totalPaid = fila[9];
					accidentIndicator = fila[12];
					violationDescription = fila[15];
					violationCode = fila[14];
					adressId = fila[3];
					streetSegId = fila[4];
					PENALTY1 = fila[10];
					PENALTY2 = fila[11];
					xCoord = fila[5];
					yCoord = fila[6];
					}

					//Se verifica la xCoord m�nima

					if(Double.parseDouble(xCoord) < xCoordMin) {

						xCoordMin = Double.parseDouble(xCoord);

					}

					//Se verifica la yCoord m�nima

					if(Double.parseDouble(yCoord) < yCoordMin) {

						yCoordMin = Double.parseDouble(yCoord);

					}

					//Se verifica la xCoord m�xima

					if(Double.parseDouble(xCoord) > xCoordMax) {

						xCoordMax = Double.parseDouble(xCoord);

					}

					//Se verifica la yCoord m�xima

					if(Double.parseDouble(yCoord) > yCoordMax) {

						yCoordMax = Double.parseDouble(yCoord);

					}

					//Se crea el objeto VOMovingViolations

					VOMovingViolations infraccion = new VOMovingViolations(objectId, location, ticketIssueDate, totalPaid, accidentIndicator, violationDescription,sumaFINEAMT, violationCode,adressId,streetSegId,PENALTY1,PENALTY2, xCoord, yCoord);


					String key = violationCode;
					if(separateChainning.contains(key))
					{
						IQueue<VOMovingViolations> lista = separateChainning.get(key);
						lista.enqueue(infraccion);
					}
					else {

						IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
						lista.enqueue(infraccion);
						separateChainning.put(key, lista);

					}


					String llave = (xCoord)+","+(yCoord);
					if(arbolBST.contains(llave))
					{
						IQueue<VOMovingViolations> lista = arbolBST.get(llave);
						lista.enqueue(infraccion);

					}else {

						IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
						lista.enqueue(infraccion);
						arbolBST.put(llave, lista);

					}

					String cadena = ticketIssueDate;
					int end = cadena.indexOf(".");
					int begin = cadena.indexOf("T");
					if(end !=-1 && begin !=-1)
					{
						cadena = cadena.substring(begin+1, end);
						LocalTime llave2 = LocalTime.parse(cadena, DateTimeFormatter.ofPattern("HH:mm:ss"));
						if(arbolBSTReqC.contains(llave2))
						{
							IQueue<VOMovingViolations> lista = arbolBSTReqC.get(llave2);
							lista.enqueue(infraccion);
							;
						}else {

							IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
							lista.enqueue(infraccion);
							arbolBSTReqC.put(llave2, lista);
						}

					}


					//	queueInfracciones.enqueue(infraccion);

					//Se agrega al arreglo la infracci�n
					listaInfracciones[pos] = infraccion;

					if (i ==0)
					{
						primerMes+= (infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					}else if(i ==1)
					{
						segundoMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					}else if(i ==2)
					{
						tercerMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					}else if(i ==3)
					{
						cuartoMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					} else if(i ==4)
					{
						quintoMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					} else if(i ==5)
					{
						sextoMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					} 

					cont++;
					pos++;
				}
				contTotal = contTotal + cont;
				cantidades[i] = cont;

				csvReader.close();

			}



			cargarEstructuras();
			System.out.println("Cantidad de infracciones por mes:");

			for(int j = 0; j < 6; j++) {

				System.out.println(meses[j] + " = " + cantidades[j]);

			}
			System.out.println("Cantidad de infracciones totales por el semestre elegido: " + contTotal);
			//			System.out.println("La coordenada m�xima en x es: " + xCoordMax);
			//			System.out.println("La coordenada m�xima en y es: " + yCoordMax);
			//			System.out.println("La coordenada m�nima en x es: " + xCoordMin);
			//			System.out.println("La coordenada m�nima en y es: " + yCoordMin);


			System.out.println("Las coordenadas m�ximas y minimas en (x,y) son: " + "("+xCoordMax+","+yCoordMax+")" + "\t" + "("+xCoordMin+","+yCoordMin+")");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


		public void cargarEstructuras() {

			ordenar();

			//Se carga la tabla hash del requerimiento 1C
			VOMovingViolations[] ordenadoA = listaInfracciones;
			VOMovingViolations aux[] = new VOMovingViolations[listaInfracciones.length];

			Sort.ordenarMergeSort(ordenadoA, aux, 0, ordenadoA.length-1, 8);

			int it = 0;

			while(it < ordenadoA.length) {

				String id = ordenadoA[it].getAdressId();
				String idActual = ordenadoA[it].getAdressId();

				String streetId = ordenadoA[it].getStreetSegId();

				if(id.equals("")) {

					id = "-1";
					idActual ="-1";

				} 

				if(streetId.equals("")) {

					streetId ="-1";

				}

				int contInfracciones = 0;
				double totalPagar = 0;
				double sinA = 0;
				double conA = 0;

				while(idActual.equals(id) && it < ordenadoA.length) {


					idActual = ordenadoA[it].getAdressId();
					totalPagar+= ordenadoA[it].getFineAmt();

					streetId = ordenadoA[it].getStreetSegId();

					if(streetId.equals("")) {

						streetId ="-1";

					}

					contInfracciones++;

					if(ordenadoA[it].getAccidentIndicator().equals("Yes")) {

						conA++;


						} else {

							sinA++;

						}


						it++;

					}


					AddressIdVO nuevo = new AddressIdVO(contInfracciones, sinA, conA, totalPagar, Integer.parseInt(streetId), Integer.parseInt(id));	
					hash1C.put(Integer.parseInt(id), nuevo);

				}



				//Se carga la maxcola prioridad del requerimiento 3C



				VOMovingViolations ordenadoC[] = listaInfracciones;
				VOMovingViolations auxC[] = new VOMovingViolations[listaInfracciones.length];
				Sort.ordenarMergeSort(ordenadoC, auxC, 0, ordenadoC.length-1, 9);


				int i = 0;
				while(i < ordenadoC.length) {


					VOMovingViolations infActual = ordenadoC[i];

					double xC = infActual.getXCoord();
					double yC = infActual.getYCoord();


					double xCActual = infActual.getXCoord();
					double yCActual = infActual.getYCoord();

					int totalInfracciones = 0;
					double pSinA = 0;
					double pConA = 0;
					String location = infActual.getLocation();
					String addressId = infActual.getLocation();
					String streetId2 = infActual.getStreetSegId();


					while(i < ordenadoC.length && (xC == xCActual && yC == yCActual) ) {

						infActual = ordenadoC[i];
						xCActual = infActual.getXCoord();
						yCActual = infActual.getYCoord();

						if(infActual.getAccidentIndicator().equals("Yes")) {

							pConA++;

						} else {

							pSinA++;

						}

						location = infActual.getLocation();
						addressId = infActual.getAdressId();
						streetId2 = infActual.getStreetSegId();

						totalInfracciones++;
						i++;
					}


					if(addressId.equals("")) {

						addressId = "-1";

					}

					if(streetId2.equals("")) {

						streetId2 = "-1";

					}


					GeoVO geoVO = new GeoVO(xC, yC, totalInfracciones, pSinA, pConA, location, addressId, streetId2);
					cola3C.agregar(geoVO);

				}

			}


	/**
	 * - Obtener el ranking de las N franjas horarias que tengan m�s infracciones. El valor N
	 *  es un dato de entrada.
	 *  Hay que usar una cola de prioridad para guardar los rangos de las horas.
	 */

	@Override
	public MaxColaPrioridad<RankingFH> rankingFranjasHorarias(int n) {
		// TODO Auto-generated method stub

		VOMovingViolations[] arregloOrdTD = listaInfracciones;
		VOMovingViolations[] aux = new VOMovingViolations[listaInfracciones.length];
		Sort.ordenarMergeSort(arregloOrdTD, aux, 0, aux.length-1, 7);


		int horaInicial = 0;
		int horaFinal = 0;
		String hora = "00";
		int it = 0;
		int[] arregloSuma = new int[24];

		MaxColaPrioridad<RankingFH> totalInfraccionesRango = new MaxColaPrioridad<>();

		for(int i = 0; i < 24; i++) {


			int contInfracciones = 0;
			double contConA = 0;
			double contSinA = 0;
			double totalPagar = 0;


			if(horaFinal > 9) {

				horaFinal = 0;
				horaInicial++;

			}

			while(hora.equals(Integer.toString(horaInicial)+Integer.toString(horaFinal)) && it < arregloOrdTD.length) {


				VOMovingViolations infActual = arregloOrdTD[it];

				String split = infActual.getTicketIssueDate().toString().split("T")[1];	
				hora = split.substring(0, 2);	


				totalPagar += infActual.getTotalPaid();


				if(infActual.getAccidentIndicator().equals("Yes")) {

					contConA++;

				} else {

					contSinA++;

				}

				it++;
				contInfracciones++;

			}

			arregloSuma[i] = contInfracciones; 

			String intervalo = Integer.toString(horaInicial)+Integer.toString(horaFinal)+":00:00-"+Integer.toString(horaInicial)+Integer.toString(horaFinal)+":59:59";

			double infraccionesTotales = (double) contInfracciones;
			double porcSinA = contSinA/infraccionesTotales;
			double porcConA = contConA/infraccionesTotales;

			RankingFH actual = new RankingFH(contInfracciones, intervalo, porcSinA, porcConA, totalPagar);
			totalInfraccionesRango.agregar(actual);


			//System.out.println(Integer.toString(horaInicial)+Integer.toString(horaFinal));

			horaFinal++;

		}		

		return totalInfraccionesRango;
	}


	/**
	 * Realizar el ordenamiento de las infracciones por Localizaci�n Geogr�fica (Xcoord,
	 * Ycoord). 
	 * @param pXcoord, coordenada en x
	 * @param pYcoord, coordenada en y	
	 * @return hash table de las infracciones que cumplan con el rango de las coordenadas
	 */
	@Override
	public hashTableSeparateChaining<String, IQueue<VOMovingViolations>> ordenLocalGeog(double pXcoord, double pYcoord) {
		// TODO Auto-generated method stub

		hashTableSeparateChaining<String, IQueue<VOMovingViolations>> hash = new hashTableSeparateChaining<>();

		for(int i = 0; i < listaInfracciones.length; i++) {

			VOMovingViolations infraccion = listaInfracciones[i];
			String key = Double.toString(infraccion.getXCoord()) + Double.toString(infraccion.getYCoord());

			if (hash.contains(key)) {

				IQueue<VOMovingViolations> lista = hash.get(key);
				lista.enqueue(infraccion);

			}
			else {

				IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
				lista.enqueue(infraccion);
				hash.put(key, lista);

			}

		}


		String key = Double.toString(pXcoord) + Double.toString(pYcoord);

		IQueue<VOMovingViolations> cola = hash.get(key);

		Iterator<VOMovingViolations> it = cola.iterator();

		//Datos a imprimir
		int totalInfracciones = 0;
		double pSinA = 0;
		double pConA = 0;
		double totalPagar = 0;
		String location ="";
		String addressId ="-1";
		String streetId ="-1";

		while(it.hasNext()) {

			VOMovingViolations actual = it.next();

			if(actual.getAccidentIndicator().equals("Yes")) {

				pConA ++;

			} else {

				pSinA++;

			}


			totalPagar += actual.getTotalPaid();
			location = actual.getLocation();


			if(!actual.getAdressId().equals("")) {
				addressId = actual.getStreetSegId();
			}

			if(!actual.getStreetSegId().equals("")) {
				streetId = (actual.getStreetSegId());
			}

			totalInfracciones++;

		}


		DecimalFormat df = new DecimalFormat("#.00"); 
		double infracciones = (double) totalInfracciones;
		double resulSinA = pSinA / infracciones;
		double resulConA = pConA / infracciones;

		String add ="";

		if(resulConA == 0) {

			add = "0";

		}


		System.out.println("Total de infracciones: "+totalInfracciones);
		System.out.println("Porcentaje de infracciones sin accidente: "+df.format(resulSinA*100)+"%");
		System.out.println("Porcentaje de infracciones con accidente: "+add+df.format(resulConA*100)+"%");
		System.out.println("Total a pagar: "+totalPagar);
		System.out.println("Locaci�n: "+location);
		System.out.println("AddressId: "+addressId);
		System.out.println("Street seg id: "+streetId);

		return hash;
	}



	/**
	 * - Buscar las infracciones por rango de fechas [Fecha Inicial (A�o/Mes/D�a), Fecha Final
	 *(A�o/Mes/D�a)]. 
	 * @param pFechaInicial, fecha inicial
	 * @param pFechaFinal, fecha final
	 * @return RedBlackBST con las infracciones dentro del rango de fechas dado
	 */
	@Override
	public RedBlackBST<String, DateVO> buscarInfRangoFechas(String pFechaInicial, String pFechaFinal) {
		// TODO Auto-generated method stub

		RedBlackBST<String, DateVO> redBlack = new RedBlackBST<>();

		VOMovingViolations[] ordenadoD = listaInfracciones;
		VOMovingViolations[] aux = new VOMovingViolations[listaInfracciones.length];

		Sort.ordenarMergeSort(ordenadoD, aux, 0, ordenadoD.length-1, 2);



		String fechaActual = ordenadoD[0].getTicketIssueDate().toString().split("T")[0];

		int cont = 0;

		while(cont < ordenadoD.length) {

			ArrayList<VOMovingViolations> lista = new ArrayList<>();

			int contInfracciones = 0;

			double sinA = 0;
			double conA = 0;
			double totalPagar = 0;

			String fecha = ordenadoD[cont].getTicketIssueDate().toString().split("T")[0];

			while(fecha.equals(fechaActual) && cont < ordenadoD.length) {


				VOMovingViolations actual = ordenadoD[cont];
				fechaActual = actual.getTicketIssueDate().toString().split("T")[0];

				if(actual.getAccidentIndicator().equals("Yes")) {

					conA++;

				} else {

					sinA++;

				}

				totalPagar += actual.getTotalPaid();

				lista.add(actual);
				cont++;
				contInfracciones++;
			}


			DateVO nuevo = new DateVO(lista, fecha, contInfracciones, sinA, conA, totalPagar);
			redBlack.put(fecha, nuevo);


		}

		return redBlack;
	}


	/**
	 * Obtener la informaci�n de una localizaci�n dada (AddressID).
	 * @param pAddressId, el addresId a consultar
	 * @return retorna un hashtable con las infracciones que cumplen el addressId
	 */
	@Override
	public AddressIdVO getLocalInfo(Integer pAddressId) {
		// TODO Auto-generated method stub

		AddressIdVO value = hash1C.get(pAddressId);		
		return value;

	}

	/**
	 * Obtener el ranking de las N localizaciones geogr�ficas (Xcoord, Ycoord) con la
	 *mayor cantidad de infracciones.
	 *@param pXcoord, coordenada en x
	 *@param pYcoord, coordenada en y
	 *@param n, cantidad de infracciones a mostrar en consola
	 *@return rertona una cola de prioridad con las infracciones dentro del rango
	 */
	@Override
	public IQueue<GeoVO> getLocalRanking(int n) {
		// TODO Auto-generated method stub

		IQueue<GeoVO> listaInfracciones = new Queue<>();
		Iterator<GeoVO> it = cola3C.iterator();

		int i = 0;

		while(it.hasNext() && i < n) {

			GeoVO actual = it.next();

			listaInfracciones.enqueue(actual);

			i++;
		}

		//		for(int i = 0; i < n; i++) {
		//			
		//			listaInfracciones.enqueue(cola3C.delMax());
		//			
		//		}
		//		
		return listaInfracciones;
	}



	/**
	 * Busqueda binaria de un arreglo
	 * @param arreglo Arreglo en el cual se va a buscar el dato
	 * @param dato Dato a buscar
	 * @return Dato encontrado
	 */
	public static int buscar( VOMovingViolations [] arreglo, String dato) {
		int inicio = 0;
		int fin = arreglo.length - 1;
		int pos;
		Sort.ordenarShellSort(arreglo, 8);
		while (inicio <= fin) {
			pos = (inicio+fin) / 2;
			if ( arreglo[pos].getViolationCode().compareTo(dato)==0 )
				return pos;
			else if ( arreglo[pos].getViolationCode().compareTo(dato) < 0 ) {
				inicio = pos+1;
			} else {
				fin = pos-1;
			}
		}
		return -1;
	}

	public  void promedio()
	{
		double sumaAMTparaPromedio=0;
		int contDat=0;
		VOMovingViolations[] aux = listaInfracciones;

		Sort.ordenarShellSort(aux, 5);
		for (int i = 0; i < aux.length; i++) {
			contDat++;
			sumaAMTparaPromedio+=aux[i].getFineAmt();
		}

		media= sumaAMTparaPromedio/(double)contDat;


	}


	/**
	 * Invertir una muestra de datos (in place).
	 * datos[0] y datos[N-1] se intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y datos[N-3] se intercambian, ...
	 * @param datos - conjunto de datos a invertir (inicio) y conjunto de datos invertidos (final)
	 */
	public void invertirMuestra( VOMovingViolations[ ] datos ) {
		// TODO implementar

		VOMovingViolations datosAux[] = new VOMovingViolations[datos.length];

		int pos = 0;
		for(int i = datos.length-1; i >= 0; i--) {

			datosAux[pos] = datos[i];
			pos++;
		}

		datos = datosAux;

	}

	/**
	 * Da los N tipos con m�s infracciones.
	 */
	@Override
	public MaxColaPrioridad<Queue<VOMovingViolations>> darRankingInfracciones(int pViolationCodes) 
	{
		Iterator<String> it = separateChainning.keys().iterator();

		while(it.hasNext())
		{
			IQueue<VOMovingViolations> llave =separateChainning.get(it.next());
			maxColaP.agregar((Queue<VOMovingViolations>) llave);
		}
		return maxColaP;
	}

	@Override
	public RedBlackBST<String,IQueue<VOMovingViolations>> consultarPorLocalizacion(double x, double y) {

		return arbolBST;
	}

	@Override
	public RedBlackBST<Integer, FineamtVO> darFranjasConValAcumulad(double valI, double valF) {

		VOMovingViolations[] arregloOrdTD = listaInfracciones;
		VOMovingViolations[] aux = new VOMovingViolations[listaInfracciones.length];
		Sort.ordenarMergeSort(arregloOrdTD, aux, 0, aux.length-1, 2);

		int hora = 0;
		int sumaFINEAMT =0;
		Queue<VOMovingViolations> lista = new Queue<VOMovingViolations>();
		RedBlackBST<Integer, FineamtVO> totalInfraccionesRango = new RedBlackBST<>();
		int cantYes =0;
		int cantNo =0;
		for (int i = 0; i < arregloOrdTD.length; i++) 
		{
			if(i==0 && arregloOrdTD[i].getTicketIssueDate().getHour()!=0) hora=6;

			if(arregloOrdTD[i].getTicketIssueDate().getHour()==hora)
			{
				sumaFINEAMT += arregloOrdTD[i].getFineAmt();
				if(arregloOrdTD[i].getAccidentIndicator().equalsIgnoreCase("Yes"))
				{

					cantYes++;
				}else
				{
					cantNo++;
				}
				lista.enqueue(arregloOrdTD[i]);

			}else
			{
				int total = cantNo+cantYes;

				if(total==0)
					total+=1;
				
				double conA= (cantNo*100)/total;
				double sinA= (cantNo*100)/total;
				FineamtVO elemento = new FineamtVO(lista, arregloOrdTD[i-1].getTicketIssueDate(), total, sinA, conA, sumaFINEAMT);
				totalInfraccionesRango.put(sumaFINEAMT, elemento);
				if(hora!=23)
				{
					hora++;	
				}else
				{
					hora=0;
				}
				cantYes=0;
				cantNo=0;
				cantYes=0;
				sumaFINEAMT=0;
				lista = new Queue<VOMovingViolations>();
			}
		}

		return totalInfraccionesRango;
	}

	@Override
	public RedBlackBST<LocalTime,IQueue<VOMovingViolations>> darInfraccionesPorRango(LocalTime pHoraInicial, LocalTime pHoraFinal) {		

		long inicio = System.currentTimeMillis();


		Iterator<LocalTime> it = arbolBSTReqC.keys(pHoraInicial, pHoraFinal).iterator();
		int contTotalInfracciones=0;
		int conA = 0;
		int sinA = 0;
		int sumaFineamt = 0;
		RedBlackBST<String, LocationVO> alternativo = new RedBlackBST<>();
		while(it.hasNext())
		{
			contTotalInfracciones++;
			LocalTime llave = it.next();
			contTotalInfracciones+=arbolBSTReqC.get(llave).size();
			Iterator<VOMovingViolations> iter =arbolBSTReqC.get(llave).iterator();
			while(iter.hasNext())
			{

				VOMovingViolations actual = iter.next();
				LocationVO elemento = new LocationVO(actual.getViolationCode(), 1, actual.getFineAmt());

				String key = actual.getViolationCode();

				if(alternativo.contains(key))
				{

					LocationVO lista = alternativo.get(key);
					lista.aumentarRegistros();
				}
				else {

					alternativo.put(key,elemento);

				}
				if(actual.getAccidentIndicator().equals("Yes"))
				{
					conA++;
				}else
				{
					sinA++;
				}
			}


		}

		int total= sinA+conA;

		long finn = System.currentTimeMillis();
		Iterator<String> corredor = alternativo.keys().iterator();
		System.out.println(" Informaion particular ");
		while(corredor.hasNext())
		{
			LocationVO detalle = alternativo.get(corredor.next());
			System.out.println("Codigo: " + detalle.getViolationCode()+ "\t"+"Cantidad de registros: " + detalle.getNumberOfRegisters());

		}

		System.out.println();

		System.out.println(" Informacion general");
		System.out.println("Total de infracciones en rango: " + contTotalInfracciones);
		System.out.println("Porcentaje infracciones con accidente: " + (conA*100)/total + "%");
		System.out.println("Porcentaje infracciones sin accidente: " + (sinA*100)/total + "%");
		System.out.println("Tiempo requerimiento 2C: " + (finn-inicio) +" milisegundos");
		return arbolBSTReqC;
	}

	public void ordenar()
	{
		Iterator<String> it = separateChainning.keys().iterator();
		int porcentaje = 0;

		while(it.hasNext())
		{
			String key = it.next();
			String cont = "";	

			if(determinante==0)
			{
				porcentaje = ((100*separateChainning.get(key).size() )/599207)+1;
			}else
			{
				porcentaje = ((100*separateChainning.get(key).size() )/664489)+1;
			}

			for (int i = 0; i < porcentaje; i++) {
				cont+="*";
			}

			if(arbolBSTReqC4.contains(cont+" = "+porcentaje))
			{
				IQueue<String> lista = arbolBSTReqC4.get(cont+" = " + porcentaje);
				lista.enqueue(key);
				;
			}else {

				IQueue<String> lista  = new Queue<String>();
				lista.enqueue(key);
				arbolBSTReqC4.put(cont+" = "+porcentaje, lista);
			}


		}

	}

	@Override
	public void getASCIITable() 
	{
		System.out.println("Reporte Agregado de Infracciones por C�digo 2018");

		while(!arbolBSTReqC4.isEmpty())
		{
			String key = arbolBSTReqC4.max();
			IQueue<String> dato = arbolBSTReqC4.get(key);

			if(dato.isEmpty())
			{
				System.out.println(arbolBSTReqC4.get(arbolBSTReqC4.max())+"|"+arbolBSTReqC4.max());
			}else
			{
				Iterator<String> it = dato.iterator();
				while(it.hasNext())
				{	
					System.out.println(it.next()+"|"+key);
				}

			}

			arbolBSTReqC4.deleteMax();		

		}

		System.out.println("Cada * representa el porcentaje sobre el total de infracciones cargadas + 1 ");


	}


}
