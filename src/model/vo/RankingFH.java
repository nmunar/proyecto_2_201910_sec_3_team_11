package model.vo;

public class RankingFH implements Comparable<RankingFH>{
	
	
	//Atributos
	
	private int totalInfracciones;
	
	private String intervalo;
	
	private double porcSinA;
	
	private double porcConA;
	
	private double totalPagar;
	
	
	//Constructor
	public RankingFH(int pTotalInfracciones, String pIntervalo, double pPorcSinA, double pPorcConA, double pTotalPagar) {
		
		totalInfracciones = pTotalInfracciones;
		intervalo = pIntervalo;
		porcSinA = pPorcSinA;
		porcConA = pPorcConA;
		totalPagar = pTotalPagar;
		
	}
	
	
	//M�todos
	
	
	public int darTotalInfracciones() {
		
		return totalInfracciones;
		
	}
	
	
	public String darIntervalo() {
		
		return intervalo;
	
	}
	
	
	public double darPorcSinA() {
		
		return porcSinA;
		
	}
	
	
	public double darPorcConA() {
		
		return porcConA;
		
	}
	
	public double darTotalPagar() {
		
		return totalPagar;
		
	}


	@Override
	public int compareTo(RankingFH o) {
		// TODO Auto-generated method stub
		return this.darTotalInfracciones()-o.darTotalInfracciones();
	}
	
	

}
