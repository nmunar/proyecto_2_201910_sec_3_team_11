package model.vo;

import java.util.ArrayList;

public class DateVO {
	
	
	//Atributos
	
	private ArrayList<VOMovingViolations> arreglo;
	
	private String fecha;
	
	private int totalInfracciones;
	
	private double sinA;
	
	private double conA;
	
	private double totalPagar;
	
	
	
	//Constructor
	
	public DateVO(ArrayList<VOMovingViolations> pArreglo, String pFecha, int pTotalInf, double pSinA, double pConA, double pTotalPagar) {
		
		arreglo = pArreglo;
		fecha = pFecha;
		totalInfracciones = pTotalInf;
		sinA = pSinA;
		conA = pConA;
		totalPagar = pTotalPagar;
		
	}
	
	
	
	//M�todos
	
	
	public ArrayList<VOMovingViolations> darInfracciones() {
		
		return arreglo;
		
	}
	
	
	public String darFecha() {
		
		return fecha;
		
	}
	
	public int darTotalInfracciones() {
		
		return totalInfracciones;
		
	}
	
	public double darSinA() {
		
		return sinA;
		
	}
	
	public double darConA() {
		
		return conA;
		
	}

	
	public double darTotalPagar() {
		
		return totalPagar;
		
	}
}
