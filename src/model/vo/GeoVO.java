package model.vo;

public class GeoVO implements Comparable<GeoVO>{
	
	
	//Atributos
	
	
	private double xCoord;
	
	private double yCoord;
	
	private int totalInfracciones;
	
	private double sinA;
	
	private double conA;
	
	private String location;
	
	private String addressId;
	
	private String streetId;
	
	
	//Constructor
	
	
	public GeoVO(double pXCoord, double pYCoord, int pTotalInfracciones, double pSinA, double pConA, String pLocation, String pAddressId, String pStreetId) {
		
		xCoord = pXCoord;
		yCoord = pYCoord;
		totalInfracciones = pTotalInfracciones;
		sinA = pSinA;
		conA = pConA;
		location = pLocation;
		addressId = pAddressId;
		streetId = pStreetId;
		
	}
	
	
	//M�todos

	
	public double darXCoord() {
		
		return xCoord;
		
	}
	
	public double darYCoord() {
		
		return yCoord;
		
	}
	
	
	public int darTotalInfracciones() 
	{
		
		return totalInfracciones;
		
	}
	
	
	public double darSinA() {
		
		return sinA;
		
	}
	
	
	public double darConA() {
		
		return conA;
		
	}
	
	
	public String darLocation() {
		
		return location;
		
	}
	
	
	public String darAddressId() {
		
		return addressId;
		
	}
	
	
	
	public String darStreetId() {
		
		return streetId;
		
	}
	
	
	//ComparadorW


	@Override
	public int compareTo(GeoVO w) {
		// TODO Auto-generated method stub
		return totalInfracciones - w.darTotalInfracciones();
	}
	
	
}
