package model.vo;

import model.data_structures.IQueue;
import model.data_structures.Queue;

public class AddressIdVO {
	
	
	//Atributos 
	
	private int cantidadInfracciones;
	
	private double sinA;
	
	private double conA;
	
	private double totalPagar;
	
	private int streetId;
	
	private int addressId;
	
	
	public AddressIdVO(int pCantidadInfracciones, double pSinA, double pConA, double pTotalPagar, int pStreetId, int pAddressId) {
		
		cantidadInfracciones = pCantidadInfracciones;
		sinA = pSinA;
		conA = pConA;
		totalPagar = pTotalPagar;
		streetId = pStreetId;
		addressId = pAddressId;
		
	}
	
	
	
	
	
	
	public int darCantidadInfracciones() {
		
		return cantidadInfracciones;
		
	}
	
	
	public double darSinA() {
		
		return sinA;
		
	}
	
	
	public double darConA() {
		
		return conA;
		
	}
	
	
	public double darTotalPagar(){
		
		return totalPagar;
		
	}
	
	
	public int darStreetId() {
		
		return streetId;
		
	}
	
	public int darAddressId() {
		
		return addressId;
		
	}
	
}
