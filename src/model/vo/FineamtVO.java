package model.vo;

import java.time.LocalDateTime;
import java.util.ArrayList;

import model.data_structures.Queue;

public class FineamtVO {
	
	
	//Atributos
	
	private Queue<VOMovingViolations> arreglo;
	
	private String fecha;
	
	private int totalInfracciones;
	
	private double sinA;
	
	private double conA;
	
	private double totalPagar;
	
	
	
	//Constructor
	
	public FineamtVO(Queue<VOMovingViolations> pArreglo, LocalDateTime pFecha, int pTotalInf, double pSinA, double pConA, double pTotalPagar) {
		
		arreglo = pArreglo;
		String year = String.valueOf(pFecha.getYear());
		String day = String.valueOf(pFecha.getDayOfMonth());
		String month = String.valueOf(pFecha.getMonth());
		String hour = String.valueOf(pFecha.getHour());
		fecha = month + "/" + day + "/" + year + "- Hour: " +hour;
		totalInfracciones = pTotalInf;
		sinA = pSinA;
		conA = pConA;
		totalPagar = pTotalPagar;
		
	}
	
	
	
	//M�todos
	
	
	public Queue<VOMovingViolations> darInfracciones() {
		
		return arreglo;
		
	}
	
	
	public String darFecha() {
		
		return fecha;
		
	}
	
	public int darTotalInfracciones() {
		
		return totalInfracciones;
		
	}
	
	public double darSinA() {
		
		return sinA;
		
	}
	
	public double darConA() {
		
		return conA;
		
	}

	
	public double darTotalPagar() {
		
		return totalPagar;
		
	}
}
