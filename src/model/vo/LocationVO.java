package model.vo;

public class LocationVO implements Comparable<LocationVO>{

	/**
	 * Atributos
	 */

	private String violationCode ; 

	private int numberOfRegisters;

	private double fineAMT = 0;

	public LocationVO( String pviolationCode, Integer pNumberOfRegisters, double pFINEAMT) {

		violationCode = pviolationCode;

		fineAMT = (pFINEAMT);


		numberOfRegisters = pNumberOfRegisters;

	}

	public double getFINEAMT()
	{
		return fineAMT;
	}

	public void aumentarRegistros()
	{
		numberOfRegisters++;
	}

	public String getViolationCode() {

		return violationCode;

	}

	public int getNumberOfRegisters() {

		return numberOfRegisters;

	}
	@Override
	public int compareTo(LocationVO o) {
		int deter = 90;

			if(o.getViolationCode().equals(violationCode))
			{
				deter = 0;
			}else if(o.getViolationCode().compareTo(violationCode) < 0)
			{
				deter = 1;
			}else if(o.getViolationCode().compareTo(violationCode) > 0)
			{
				deter = -1;
			}

		return deter;
	}

}
