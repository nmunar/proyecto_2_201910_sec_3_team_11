package model.data_structures;
import java.util.Iterator;

import model.vo.VOMovingViolations;
public class Queue<Key> implements IQueue<Key> , Comparable<Queue<Key>>
{
	private Node<Key> primero;
	private Node<Key> ultimo;
	private Node<Key> actual;
	private int longitud;

	public Queue() 
	{ 
		longitud = 0 ;
		primero = null ;
		ultimo = null;
		actual=primero;
	} 

	@Override
	public Iterator iterator() {
		return new IteratorSQ<Key>(primero);
	}

	@Override
	public boolean isEmpty() 
	{ 
		return primero==null;
	}

	@Override
	public int size() 
	{
		return longitud;
	}

	@Override
	public void enqueue(Key t) 
	{  
		Node<Key> agregado = new Node<Key>((Key) t);
		if(primero==null)
		{  primero =  agregado;
		ultimo= agregado;
		longitud++;
		}else{
			agregado.cambiarAnterior(ultimo);
			ultimo.cambiarSiguiente(agregado);
			ultimo = agregado;
			longitud++;
		} 
	}

	public Node<Key> darUltimo()
	{
		return ultimo;
	}

	public Node<Key> darPrimero()
	{
		return primero;
	}
	public Node<Key> darActual()
	{
		return actual;
	}

	@Override
	public Key dequeue() 
	{  
		Node<Key> actual = primero;
		Node<Key> siguiente = actual.darSiguiente();
		Key retornar = actual.darElemento();
		if(primero==null)
		{  }else  { 
			siguiente.cambiarAnterior(null);
			primero = siguiente;
			retornar = actual.darElemento();
			longitud--;
		}
		return retornar;

	}

	@Override
	public int compareTo(Queue arg0) 
	{
		return this.size() -arg0.size();
	}

}
