package controller;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

import com.opencsv.ResultSetColumnNameHelperService;
import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;

import api.IMovingViolationsManager;
import javafx.geometry.VPos;
import model.data_structures.IQueue;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.hashTableSeparateChaining;
import model.logic.MovingViolationsManager;
import model.vo.AddressIdVO;
import model.vo.DateVO;
import model.vo.FineamtVO;
import model.vo.GeoVO;
import model.vo.RankingFH;
import model.vo.LocationVO;
import model.vo.RankingFH;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	private static IMovingViolationsManager  manager = new MovingViolationsManager();

	private int option2 = 0;

	private boolean carga = false;


	public Controller() {
		view = new MovingViolationsManagerView();

		//TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		long startTime;
		long endTime;
		long duration;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:

				if(carga == false) {

					view.printMenuSem();
					option2 = sc.nextInt();
					carga = true;

					//					long inicio = System.currentTimeMillis();
					this.loadMovingViolations();
					//					long finn = System.currentTimeMillis();
					//					long duracion = finn - inicio;
					//
					//					System.out.println("Se demora en cargar: " +  duracion + " milisegundos.");


				} else {
					System.out.println("No se permite cargar los archivos más de una vez.");
					System.out.println("");
				}

				break;

			case 2:

				if(carga == true) {

					System.out.println("Consultar las N franjas horarias con mas infracciones que desea ver. Ingresar valor de N (1-24): ");
					int numeroFranjas = sc.nextInt();
					MaxColaPrioridad<RankingFH> rankingFranjas = darRankingFranjasHorarias(numeroFranjas);

					System.out.println("Intervalo de horas" + "\t" + "Cantidad de infracciones" + "\t" + "Porcentaje sin accidente %" + "\t" + "Porcentaje con accidente %" + "\t" + "Total a pagar \n");
					DecimalFormat df = new DecimalFormat("#.00");
					for(int i = 0; i < numeroFranjas; i++) {

						String add = "";
						RankingFH actual = rankingFranjas.delMax();

						if(actual.darPorcConA()*100 < 1) {

							add = "0";

						}

						System.out.println(actual.darIntervalo() + "\t\t" + actual.darTotalInfracciones() + "\t\t\t\t" +  df.format(actual.darPorcSinA()*100)+"%" + "\t\t\t\t" + add + df.format((actual.darPorcConA()*100))+"%" + "\t\t\t " + "$"+actual.darTotalPagar());

					}


					//					Iterator<RankingFH> it = darRankingFranjasHorarias(numeroFranjas).iterator();
					//					int total = 0;
					//					
					//					while(it.hasNext()) {
					//						
					//						RankingFH actual = it.next();
					//						total += actual.darTotalInfracciones(); 
					//						
					//					}
					//					
					//					System.out.println("/////////");
					//					System.out.println("Total: " + total );

					System.out.println("");
				}

				else {
					System.out.println("Debe primero cargar un semestre.");
				}

				break;
			case 3:

				if(carga == true) {


					view.printMessage("Ingrese la coordenada en X de la localizacion geografica (Ej. 1234,56): ");
					double xcoord = sc.nextDouble();
					view.printMessage("Ingrese la coordenada en Y de la localizacion geografica (Ej. 5678,23): ");
					double ycoord = sc.nextDouble();

					ordenLocalGeog(xcoord, ycoord);


				}

				else {
					System.out.println("Debe primero cargar un semestre.");
				}

				break;

			case 4:

				if(carga == true) {

					view.printMessage("Ingrese la fecha inicial del rango. Formato año-mes-dia (ej. 2008-06-21)");
					String fechaInicialStr = sc.next();
					//LocalDate fechaInicial = ManejoFechaHora.convertirFecha_LD( fechaInicialStr );

					view.printMessage("Ingrese la fecha final del rango. Formato año-mes-dia (ej. 2008-06-30)");
					String fechaFinalStr = sc.next();
					//LocalDate fechaFinal = ManejoFechaHora.convertirFecha_LD( fechaFinalStr );


					RedBlackBST<String, DateVO> redBlack = buscarInfRangoFechas(fechaInicialStr, fechaFinalStr);					

					Iterator<String> it = redBlack.keys(fechaInicialStr, fechaFinalStr).iterator();

					DecimalFormat df = new DecimalFormat("#.00"); 
					while(it.hasNext()) {

						DateVO date = redBlack.get(it.next());

						ArrayList<VOMovingViolations> array = date.darInfracciones();	
						String add = "";



						System.out.println("Fecha (Año/Mes/Día): " + date.darFecha());
						System.out.println("Total de infracciones: " + 	date.darTotalInfracciones());
						double totInf = (double) date.darTotalInfracciones();
						double resulSinA = date.darSinA() / totInf;
						double resulConA = date.darConA() / totInf;

						if(resulConA < 1) {

							add = "0";

						}

						System.out.println("Porcentaje sin accidente: " + df.format((resulSinA)*100)+"%");
						System.out.println("Porcentaje con accidente: " + add + df.format((resulConA)*100)+"%");
						System.out.println("Total a pagar: " + "$"+date.darTotalPagar());

						System.out.print("\n");


					}



				} else {

					System.out.println("Debe primero cargar un semestre.");

				}
				break;

			case 5:

				if(carga == true) {
					view.printMessage("1B. Consultar los N Tipos con mas infracciones. Ingrese el valor de N: ");
					int numeroTipos = sc.nextInt();
					MaxColaPrioridad<Queue<VOMovingViolations>> var = darRankingInfracciones(numeroTipos);


					System.out.println("ViolationCode \t Total Infracciones \t % Infracciones con accidente \t % Infracciones sin accidente \t ValorTotal");

					for (int i = 0; i < numeroTipos; i++) {
						Queue<VOMovingViolations> actual = var.delMax(); 
						Iterator<VOMovingViolations> it = actual.iterator();
						int contVal = 0;
						int cont1 = 0;
						int cont2 = 0;
						while(it.hasNext())
						{
							VOMovingViolations dato = it.next();
							contVal += dato.getFineAmt();
							if(dato.getAccidentIndicator().equals("Yes"))
							{
								cont1++;
							}else if(dato.getAccidentIndicator().equals("No"))
							{
								cont2++;
							}
						}
						int total = cont1+cont2;

						System.out.println( i+1+"."+ actual.dequeue().getViolationCode() +  "\t \t \t " + actual.size() +  "\t \t \t" + ((cont1*100)/total)+"%" +  "\t \t \t \t  " + ((cont2*100)/total)+"%" +  "\t \t \t " + "$"+contVal);

					}

				} else 
				{
					System.out.println("Debe primero cargar un semestre.");
				}

				break;

			case 6:

				if(carga == true) {

					double xcoord =0;
					double ycoord =0;
					view.printMessage("Ingrese la coordenada en X de la localizacion geografica (Ej. 401644,78): ");
					xcoord = sc.nextDouble();
					view.printMessage("Ingrese la coordenada en Y de la localizacion geografica (Ej. 137368,36): ");
					ycoord = sc.nextDouble();

					String llave = String.valueOf(xcoord)+","+String.valueOf(ycoord);

					RedBlackBST<String, IQueue<VOMovingViolations>> arbol =consultarPorLocalizacionArbol(xcoord,ycoord);
					IQueue<VOMovingViolations> val =arbol.get(llave);
					Iterator<VOMovingViolations> iter = val.iterator();
					int accidente = 0;
					int noAccidente = 0;
					int sumaFINEAMT=0;

					VOMovingViolations actual = iter.next();

					while(iter.hasNext())
					{

						if(actual.getAccidentIndicator().equals("Yes"))
						{
							accidente++;
						}else if(actual.getAccidentIndicator().equals("No"))
						{
							noAccidente++;
						}

						sumaFINEAMT+= actual.getFineAmt();

						iter.next();

					}

					int total = accidente+noAccidente;

					System.out.println("Location: " + actual.getLocation() );
					System.out.println("Address ID : " + actual.getAdressId() );
					System.out.println("StreetSegId: " + actual.getStreetSegId() );
					System.out.println("Total infracciones: "+val.size());
					System.out.println("Porcentaje infracciones con Accidente: "+ accidente*100/total +"%");
					System.out.println("Porcentaje infracciones sin Accidente: "+ noAccidente*100/total +"%");
					System.out.println("Valor total a pagar: "+ sumaFINEAMT +"$");


				} else {
					System.out.println("Debe primero cargar un semestre.");
				}

				break;

			case 7:
				if(carga == true) {

					view.printMessage("Ingrese la cantidad minima de dinero que deben acumular las infracciones en sus rangos de fecha  (Ej. 1234,56)");
					double cantidadMinima = sc.nextDouble();

					view.printMessage("Ingrese la cantidad maxima de dinero que deben acumular las infracciones en sus rangos de fecha (Ej. 5678,23)");
					double cantidadMaxima = sc.nextDouble();

					RedBlackBST<Integer, FineamtVO> violationCodes = darFranjasConValAcumulado(cantidadMinima, cantidadMaxima);

					Iterator<Integer> it = violationCodes.keys((int)cantidadMinima, (int)cantidadMaxima).iterator();
					System.out.println("");
					while(it.hasNext())
					{
						int llave = it.next();
						System.out.println("Fecha: " + violationCodes.get(llave).darFecha());
						System.out.println("Valor de infracciones Acumulado: " +"$"+violationCodes.get(llave).darTotalPagar());
						System.out.println("Total Infracciones: " + violationCodes.get(llave).darTotalInfracciones());
						System.out.println("Procentaje Infracciones sin accidente: "+violationCodes.get(llave).darSinA()+"%");
						System.out.println("Procentaje Infracciones con accidente: "+violationCodes.get(llave).darConA()+"%");
						System.out.println("");
					}

				}else {
					System.out.println("Debe primero cargar un semestre.");
				}
				break;

			case 8:
				if(carga == true) {

					System.out.println("Ingrese el addressId a consultar");
					int id = sc.nextInt();
					DecimalFormat df = new DecimalFormat("#.00"); 
					
					long inicioM = System.currentTimeMillis();
					long inicio = System.nanoTime();
					AddressIdVO value = getLocalInfo(id);
					long finn = System.nanoTime();
					long finM = System.currentTimeMillis();


					System.out.println("Tiempo: " + (finn-inicio) + " ns");
					System.out.println("Tiempo: " + (finM-inicioM) + " ms");


					double total = (double) value.darCantidadInfracciones();
					double pSinA = value.darSinA() / total;
					double pConA = value.darConA() / total;


					String add ="";

					if(pConA < 1) {

						add = "0";

					}

					System.out.println("Address id: " + value.darAddressId());
					System.out.println("Total de infracciones: " + value.darCantidadInfracciones());
					System.out.println("Porcentaje de infracciones sin accidente: " + df.format(pSinA*100) +"%");
					System.out.println("Porcentaje de infracciones con accidente: " + add+ df.format(pConA*100) +"%");
					System.out.println("Cantidad total a pagar por las infracciones: "+value.darTotalPagar());
					System.out.println("Street segment id: " + value.darStreetId());		

				}else{
					System.out.println("Debe primero cargar un semestre.");
				}
				break;

			case 9:

				if(carga == true) {

					view.printMessage("Ingrese la hora inicial del rango. Formato HH:MM:SS (ej. 09:30:00)");
					String horaInicialStr = sc.next();
					LocalTime horaInicial = convertirHora_LT(horaInicialStr);

					view.printMessage("Ingrese la hora final del rango. Formato HH:MM:SS (ej. 16:00:00)");
					String horaFinalStr = sc.next();
					LocalTime horaFinal = convertirHora_LT(horaFinalStr);

					startTime = System.currentTimeMillis();

					darInfraccionesEnRango(horaInicial, horaFinal);

					endTime = System.currentTimeMillis();
					//startTime = System.currentTimeMillis();
					
					darInfraccionesEnRango(horaInicial, horaFinal);
					
					//endTime = System.currentTimeMillis();

					//duration = endTime - startTime;
					//view.printMessage("Tiempo requerimiento 2C: " + duration + " milisegundos");


				} else {

					System.out.println("Debe primero cargar un semestre.");
				}

				break;

			case 10:

				if(carga == true) {
					
					
					System.out.println("Consultar las N localizaciones geograficas con mas infracciones. Ingrese el valor de N: ");
					int cantidad = sc.nextInt();
					
					DecimalFormat df = new DecimalFormat("#.00");
					
					long inicio = System.nanoTime();
					long inicioM = System.currentTimeMillis();
					IQueue<GeoVO> lista = getLocalRanking(cantidad);
					long end = System.nanoTime();
					long endM = System.currentTimeMillis();
					
					System.out.println("Tiempo: " + (end-inicio) + " ns");
					System.out.println("Tiempo: " + (endM-inicioM) + " ms");
					System.out.println("");
					
					Iterator<GeoVO> it = lista.iterator();
					
					
					while(it.hasNext()) {
						
						GeoVO actual = it.next();
						
						double totInfracciones = (double) actual.darTotalInfracciones();
						double pSinA = actual.darSinA()/totInfracciones;
						double pConA = actual.darConA()/totInfracciones;
						String add = "";
						
						if(pConA < 1) {
							
							add = "0";
							
						}
						
						System.out.println("Coordenada en x: "+actual.darXCoord());
						System.out.println("Coordenada en y: "+actual.darYCoord());
						System.out.println("Total de infracciones: " + actual.darTotalInfracciones());
						System.out.println("Porcentaje de infracciones con accidente: " + add + df.format(pConA*100) + "%");
						System.out.println("Porcentaje de infracciones sin accidente: " + df.format(pSinA*100) + "%");
						System.out.println("Locación: " + actual.darLocation());
						System.out.println("AddressId: " + actual.darAddressId());
						System.out.println("Street id: " + actual.darStreetId());
						System.out.println("");
						
					}
					
				} 
				else {
					System.out.println("Debe primero cargar un cuatrimestre.");
				}

				break;

			case 11: 

				if(carga == true) {
					
					
					System.out.println("Grafica ASCII con la informacion de las infracciones por ViolationCode");

					startTime = System.currentTimeMillis();
					getASCII();
					endTime = System.currentTimeMillis();

					duration = endTime - startTime;
					view.printMessage("Tiempo requerimiento 4C: " + duration + " milisegundos");
				

				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");
				}
				break;

			case 12:	

				fin=true;
				sc.close();

				break;

			}
		}
	}

	public MaxColaPrioridad<Queue<VOMovingViolations>>darRankingInfracciones(int pViolationCodes) {
		return manager.darRankingInfracciones(pViolationCodes);
	}

	public hashTableSeparateChaining<String, IQueue<VOMovingViolations>> ordenLocalGeog(double pXCoord, double pYcoord) {
		return manager.ordenLocalGeog(pXCoord, pYcoord);
	}

	public RedBlackBST<String, DateVO> buscarInfRangoFechas(String pFechaInicial, String pFechaFinal) {
		return manager.buscarInfRangoFechas(pFechaInicial, pFechaFinal);
	}

	public AddressIdVO getLocalInfo(Integer pAddressId) {
		// TODO Auto-generated method stub
		return manager.getLocalInfo(pAddressId);
	}

	public IQueue<GeoVO> getLocalRanking(int n) {
		// TODO Auto-generated method stub
		return manager.getLocalRanking(n);
	}


	public RedBlackBST<String,IQueue<VOMovingViolations>> consultarPorLocalizacionArbol(double x, double y) {
		return manager.consultarPorLocalizacion( x, y);
	}


	public RedBlackBST<Integer, FineamtVO> darFranjasConValAcumulado(double limiteInf, double limiteSup) {
		return manager.darFranjasConValAcumulad(limiteInf, limiteSup);
	}

	public RedBlackBST<LocalTime,IQueue<VOMovingViolations>> darInfraccionesEnRango(LocalTime pHoraInicial, LocalTime pHoraFinal) {
		return manager.darInfraccionesPorRango(pHoraInicial, pHoraFinal);	
	}
	public void getASCII() {
		manager.getASCIITable();
	}


	public MaxColaPrioridad<RankingFH> darRankingFranjasHorarias(int n) {

		return manager.rankingFranjasHorarias(n);

	}

	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora){
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}

	public  void loadMovingViolations() {
		manager.loadMovingViolations(option2, 1);
	}

	public static LocalTime convertirHora_LT(String hora)
	{
		return LocalTime.parse(hora, DateTimeFormatter.ofPattern("HH:mm:ss"));
	}

}
