package view;

import model.data_structures.IQueue;
import model.data_structures.RedBlackBST;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{

	}

	public void printMenu() {

		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto 2----------------------");
		System.out.println("1. Cargar datos del semestre");
		
		System.out.println("2. Obtener el ranking de las N franjas horarias que tengan mas infracciones.");
		System.out.println("3. Realizar  el  ordenamiento  de  las  infracciones  por  Localizacion  Geografica.");
		System.out.println("4. Buscar las infracciones por rango de fechas");		
		
		System.out.println("5. Obtener  el  ranking  de  las  N  tipos  de  infraccion  (ViolationCode)  que  tengan  mas infracciones.");
		System.out.println("6. Realizar  el  ordenamiento  de  las  infracciones  por  Localizacion  Geografica. Implementando un arbol");
		System.out.println("7. Buscar las franjas de fecha-hora donde se tiene un valor acumulado de infracciones en un rango dado.");
		
		System.out.println("8. Obtener información de una locaclización dada");
		System.out.println("9. Obtener infracciones en un rango de horas");
		System.out.println("10. Obtener el ranking de las N locaclizaciones geográficas con mayor cant de infracciones");
		System.out.println("11. Mostrar  una  gráfica ASCII con la  información  de  las infracciones  por  código");
		
		System.out.println("12. Salir");
		System.out.println("Digite el número de opción para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMenuSem() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto_2----------------------");
		System.out.println("1. Enero, Febrero, Marzo, Abril, Mayo, Junio");
		System.out.println("2. Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre");
		System.out.println("Digite el número de opción para elegir el semestre del año que desea cargar, luego presione enter: (Ej., 1):");

	}


	public void printMovingViolationsReq7(IQueue<VOMovingViolations> resultados7) {
		System.out.println("OBJECTID"+"\t" +"TICKETISSUEDAT"+"\t"+"VIOLATIONDESC");
		for(VOMovingViolations v: resultados7) {
			System.out.println( v.objectId() + "\t" + v.getTicketIssueDate() + "\t" + v.getViolationDescription());
		}
	}
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}


}
