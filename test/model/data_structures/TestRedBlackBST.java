package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.vo.VOMovingViolations;

public class TestRedBlackBST {

	private RedBlackBST<String, Integer> st;
	String test ="";
	String[] keys = new String[0];

	private RedBlackBST<Integer,IQueue<VOMovingViolations>> redblack2 = new RedBlackBST<Integer,IQueue<VOMovingViolations>>();

	
	public void escenario1() {
		
		st = new RedBlackBST<String, Integer>();

		test = "S E A R C H E X A M P L E"; 
		keys = test.split(" "); 


	}
	
	public void escenarioExtremeBST()
	{
		for (int i = 0; i < 10000; i++) 
		{
			VOMovingViolations dats = new VOMovingViolations(Integer.toString(i), "Louisiana", "2018-03-24T18:55:00.000Z","0","T120","Choque","0" ,"3456789" ,Integer.toString(i),"456789","0","0","20","30");

			int keyAdressId = Integer.parseInt(dats.getAdressId());

			if (redblack2.contains(keyAdressId)) {


				IQueue<VOMovingViolations> lista = redblack2.get(keyAdressId);
				lista.enqueue(dats);

			}
			else {

				IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
				lista.enqueue(dats);
				redblack2.put(keyAdressId, lista);


			}
		}
	}

	@Test
	public void testSize() {

		escenario1();	
		
		for (int i = 0; i < keys.length; i++) {
            st.put(keys[i], i);
		}
		
		
		assertEquals(10, st.size());

	}
	
	@Test
	public void testGet() {
		escenario1();
		
		for (int i = 0; i < keys.length; i++) {
            st.put(keys[i], i);
		}
		
		
		int obtenido = st.get("S");
		
		assertEquals(0, obtenido);
		
	}
	
	
	
	@Test
	public void testDelete() {
		
		escenario1();
		
		for (int i = 0; i < keys.length; i++) {
            st.put(keys[i], i);
		}
		
		
		assertEquals(10, st.size());
		
		st.delete("S");
		
		assertEquals(9, st.size());
		
		
	}
	
	@Test
	public void testSizeBST() {

		escenarioExtremeBST();	
		
		assertEquals(10000, redblack2.size());

	}
	
	
	@Test
	public void testDeleteBST() {
		
		escenarioExtremeBST();		
		
		assertEquals(10000, redblack2.size());
		
		redblack2.delete(1);
		
		assertEquals(9999, redblack2.size());
		
		
	}
	
}

