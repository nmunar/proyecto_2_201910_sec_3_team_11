package model.data_structures;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;
import model.vo.VOMovingViolations;

public class SortTest 
{

	// Muestra de datos a ordenar
	private VOMovingViolations[] datos;
		
	public void escenario1()
	{
		datos= new VOMovingViolations[6];
		VOMovingViolations dat1 = new VOMovingViolations("3", "2", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A","2","2","2","2","2");
		datos[0]=dat1;
		VOMovingViolations dat2 = new VOMovingViolations("5", "e", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A","2","2","2","2","2");
		datos[1]=dat2;
		VOMovingViolations dat3 = new VOMovingViolations("7", "e", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A","2","2","2","2","2");
		datos[2]= dat3;
		VOMovingViolations dat4 = new VOMovingViolations("10", "e", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A","2","2","2","2","2");
		datos[3]=dat4;
		VOMovingViolations dat5 = new VOMovingViolations("8", "e", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A","2","2","2","2","2");
		datos[4]=dat5;
		VOMovingViolations dat6 = new VOMovingViolations("1", "e", "2018-03-24T18:55:00.000Z","0","T120","v", "2","1" , "A","2","2","2","2","2");
		datos[5]=dat6;

	}

	@Test
	public void testMerge() {
		escenario1();
		VOMovingViolations[]aux=new VOMovingViolations[datos.length];
		Sort.ordenarMergeSort(datos, aux, 0, datos.length-1,0);
		for (int i = 0; i < aux.length-1; i++) {
			if(datos[i].compareTo(datos[i+1])>1)
			{
				fail("No qued� ordenado el arreglo");
			}
		}
		
	}
	}