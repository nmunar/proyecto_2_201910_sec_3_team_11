package model.data_structures;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import model.data_structures.IMaxColaPrioridad;
import model.data_structures.IMaxHeapCP;
import model.data_structures.MaxColaPrioridad;
import model.data_structures.MaxHeapCP;
import model.vo.LocationVO;

public class PruebaColaHeapPrioridad
{	
	private LocationVO[] datos2;

	private LocationVO[] datos3;

	
	private IMaxColaPrioridad<LocationVO> cola = new MaxColaPrioridad<LocationVO>();
	
	
	private IMaxColaPrioridad<LocationVO> cola2 = new MaxColaPrioridad<LocationVO>();


	private IMaxHeapCP<LocationVO> heap = new MaxHeapCP<LocationVO>();
	
	private IMaxHeapCP<LocationVO> heap2 = new MaxHeapCP<LocationVO>();


	public void escenario1()
	{
		datos2 = new LocationVO[6];
		LocationVO dat1 = new LocationVO("a",1, 0);
		datos2[0]=dat1;
		LocationVO dat2 = new LocationVO("b",2, 0);
		datos2[1]=dat2;
		LocationVO dat3 = new LocationVO("c",3, 0);
		datos2[2]=dat3;
		LocationVO dat4 = new LocationVO("d",4, 0);
		datos2[3]=dat4;
		LocationVO dat5 = new LocationVO("e",5, 0);
		datos2[4]=dat5;
		LocationVO dat6 = new LocationVO("f",6, 0);
		datos2[5]=dat6;

	}
	
	public void escenario2()
	{
		datos3 = new LocationVO[6];
		LocationVO dat1 = new LocationVO("a",3, 0);
		datos3[0]=dat1;
		LocationVO dat2 = new LocationVO("b",300,0);
		datos3[1]=dat2;
		LocationVO dat3 = new LocationVO("c",10,0);
		datos3[2]=dat3;
		LocationVO dat4 = new LocationVO("d",15,0);
		datos3[3]=dat4;
		LocationVO dat5 = new LocationVO("e", 6,0);
		datos3[4]=dat5;
		LocationVO dat6 = new LocationVO("f",1,0);
		datos3[5]=dat6;

	}

	@Test
	public void testAgregarHeap() 
	{
		escenario1();
		for (int i = 0; i < datos2.length; i++) 
		{
			heap.agregar(datos2[i]);
		}
		assertTrue(heap.darNumElementos()==6);
		
		escenario2();
		for (int i = 0; i < datos3.length; i++) 
		{
			heap2.agregar(datos3[i]);
		}
		assertTrue(heap2.darNumElementos()==6);
		
	}
	

	@Test
	public void testDelMaxHeap() {

		testAgregarHeap();
		assertEquals(datos2[5], heap.delMax());
		assertEquals(datos3[1], heap2.delMax());
		
	}

	@Test
	public void testAgregarQueue() {
		escenario1();
		for (int i = 0; i < datos2.length; i++) 
		{
			cola.agregar(datos2[i]);
		}
		assertTrue(cola.darNumElementos()==6);
		
		escenario2();
		for (int i = 0; i < datos3.length; i++) 
		{
			cola2.agregar(datos3[i]);
		}
		assertTrue(cola2.darNumElementos()==6);

	}

	@Test
	public void testDelMaxQueue() {
		testAgregarQueue();
		assertTrue(cola.delMax().equals(datos2[5]));
		assertTrue(cola2.delMax().equals(datos3[1]));

	}




}
