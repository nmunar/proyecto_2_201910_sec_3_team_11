package model.data_structures;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.*;
import org.junit.Test;

import model.data_structures.IQueue;
import model.data_structures.Queue;
import model.data_structures.hashTableLinearProbing;
import model.data_structures.hashTableSeparateChaining;
import model.vo.VOMovingViolations;

public class linearProbingAndTestSeparateChainningTest 
{
	private VOMovingViolations[] datos;

	private VOMovingViolations[] datos1;

	private hashTableLinearProbing<Integer,VOMovingViolations> linear = new hashTableLinearProbing<Integer,VOMovingViolations>();
	private hashTableLinearProbing<Integer,IQueue<VOMovingViolations>> linear2 = new hashTableLinearProbing<Integer,IQueue<VOMovingViolations>>();
	private hashTableSeparateChaining<Integer,VOMovingViolations> separate = new hashTableSeparateChaining<Integer,VOMovingViolations>();
	private hashTableSeparateChaining<Integer,IQueue<VOMovingViolations>> separate2 = new hashTableSeparateChaining<Integer,IQueue<VOMovingViolations>>();

	private IQueue<VOMovingViolations> extreme;


	public void escenarioExtremeLin()
	{
		for (int i = 0; i < 10000; i++) 
		{
			VOMovingViolations dats = new VOMovingViolations(Integer.toString(i), "Louisiana", "2018-03-24T18:55:00.000Z","0","T120","Choque","0" ,"3456789" ,Integer.toString(i),"456789","0","0","20","30");

			int keyAdressId = Integer.parseInt(dats.getAdressId());

			if (linear2.contains(keyAdressId)) {


				IQueue<VOMovingViolations> lista = linear2.get(keyAdressId);
				lista.enqueue(dats);

			}
			else {

				IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
				lista.enqueue(dats);
				linear2.put(keyAdressId, lista);


			}
		}
	}
	
	public void escenarioExtremeSep()
	{
		for (int i = 0; i < 10000; i++) 
		{
			VOMovingViolations dats = new VOMovingViolations(Integer.toString(i), "Louisiana", "2018-03-24T18:55:00.000Z","0","T120","Choque","0" ,"3456789" ,Integer.toString(i),"456789","0","0","20","30");
			int keyAdressId =Integer.parseInt(dats.getAdressId());

			if (separate2.contains(keyAdressId)) {

				IQueue<VOMovingViolations> lista = separate2.get(keyAdressId);
				lista.enqueue(dats);

			}
			else {

				IQueue<VOMovingViolations> lista  = new Queue<VOMovingViolations>();
				lista.enqueue(dats);
				separate2.put(keyAdressId, lista);


			}
		}
	}

	@Test
	public void testPutExtremeLinearProbing() 
	{
		long inicio = System.currentTimeMillis();
		escenarioExtremeLin();
		long finTiempo = System.currentTimeMillis();
		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de put() LinaerProbing es : \n");
		System.out.println(finTiempo-inicio);
		assertTrue(linear2.size()==10000);
	}

	@Test
	public void testPutExtremeSeparateChainning() 
	{
		long inicio = System.currentTimeMillis();
		escenarioExtremeSep();
		long finTiempo = System.currentTimeMillis();
		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de put() SeparateChainnig es : \n");
		System.out.println(finTiempo-inicio);
		assertTrue(separate2.size()==10000);
	}

	@Test
	public void getExtremeLinearProbing()
	{
		escenarioExtremeLin();
		long inicio = System.currentTimeMillis();
		for (int i = 0; i < linear2.size(); i++) 
		{
			linear2.get(i);
		}
		long finTiempo = System.currentTimeMillis();
		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de get () LinaerProbing es : \n");
		System.out.println(finTiempo-inicio);
	}


	@Test
	public void getExtremeSeparateChainning()
	{
		escenarioExtremeSep();
		long inicio = System.currentTimeMillis();
		for (int i = 0; i < separate2.size(); i++) 
		{
			separate2.get(i);
		}
		long finTiempo = System.currentTimeMillis();
		System.out.println("-----------------------------------------------------------");
		System.out.println("El tiempo de demora de get() SeparateChainning es : \n");
		System.out.println(finTiempo-inicio);


	}


	@Test
	public void testDeleteLinearProbing() {

		VOMovingViolations dats = new VOMovingViolations("1", "Louisiana", "2018-03-24T18:55:00.000Z","0","T120","Choque","0" ,"3456789" ,"1","456789","0","0","20","30");
		linear.put(22, dats);

		assertEquals(1, linear.size());

		linear.delete(22);

		assertEquals(0, linear.size());


	}

	@Test
	public void testDeleteSeparateChaining() {

		VOMovingViolations dats = new VOMovingViolations("1", "Louisiana", "2018-03-24T18:55:00.000Z","0","T120","Choque","0" ,"3456789" ,"1","456789","0","0","20","30");
		separate.put(22, dats);

		assertEquals(1, separate.size());

		separate.delete(22);

		assertEquals(0, separate.size());


	}

}
