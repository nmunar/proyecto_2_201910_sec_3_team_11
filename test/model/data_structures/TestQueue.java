package model.data_structures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class TestQueue extends TestCase{

	private Queue<Integer> numeros;
	@Before
	public void setUp() throws Exception
	{
		numeros  = new Queue<Integer>();
		
		numeros.enqueue(1);
		numeros.enqueue(2);
		numeros.enqueue(3);
		numeros.enqueue(4);
		numeros.enqueue(5);
	}
	
	@Test
	public void test()
	{
		assertEquals(5, numeros.size());

		assertEquals(1, numeros.dequeue().intValue());

		numeros.dequeue();
		assertEquals(5, numeros.size());
	}

}
